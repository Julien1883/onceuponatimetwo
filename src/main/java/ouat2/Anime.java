package ouat2;

public class Anime {
    String anime;
    String character;
    String quote;

    public Anime(String anime, String character, String quote) {
        this.anime = anime;
        this.character = character;
        this.quote = quote;
    }


    @Override
    public String toString() {
        return
                "-_- "+ '\n' +
                "Название аниме: " + anime + '\n' +
                "Герой: " + character + '\n' +
                "Цель жизни: '" + quote + '\'';
    }
}
