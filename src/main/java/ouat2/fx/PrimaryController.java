package ouat2.fx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import ru.kpfu.itis.akhmetova.test2.HttpClientParseData;

import java.net.MalformedURLException;

public class PrimaryController {

    @FXML
    private TextField busInput;

    @FXML
    private Text text;

    private final String busAPI = "https://animechan.vercel.app/api/random";

    @FXML
    void getBusData(ActionEvent event) throws MalformedURLException {
        HttpClientParseData httpClientParseData = new HttpClientParseData();

        text.setText(httpClientParseData.getDataAboutBus(busAPI));
    }
}